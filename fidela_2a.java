package Fidela_2a;

import java.util.Scanner;

public class fidela_2a {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("##  Program Java Menghitung Keliling Lingkaran  ##");
		System.out.println("=============================================");
		System.out.println();

		double k, r, phi = 3.14;
		Scanner scan = new Scanner(System.in);

		System.out.print("Masukkan panjang jari-jari lingkaran : ");
		r = scan.nextDouble();

		k = 2 * phi * r;

		System.out.println("Keliling Lingkaran adalah : " + k);
	}

}
