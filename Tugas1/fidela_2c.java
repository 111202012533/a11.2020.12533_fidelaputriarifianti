package Fidela_2c;

import java.util.Scanner;

public class fidela_2c {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("##  Program Java Menghitung Volume Tabung  ##");
		System.out.println("=============================================");
		System.out.println();

		Scanner input = new Scanner(System.in);
		double hasil1, hasil2, d, t;
		double phi = 3.14;

		System.out.print("Masukkan Diameter : ");
		d = input.nextDouble();
		System.out.print("Masukkan Tinggi : ");
		t = input.nextDouble();

		hasil2 = d / 2;
		hasil1 = (phi * hasil2 * hasil2 * t);

		System.out.println("Hasil Perhitungannya adalah : " + hasil1);
	}

}
